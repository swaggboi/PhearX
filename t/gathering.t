use Mojo::Base -strict;

use Test::More;
use Test::Mojo;

my $t = Test::Mojo->new('PhearX');

$t->get_ok('/')->status_is(302)->header_like(Location => qr/version/);
$t->get_ok('/version')->status_is(200)->text_like(h2 => qr/DB Version/);

subtest 'Book new event', sub {
    $t->get_ok('/book')->status_is(200)
        ->element_exists('form input[name="author_name"]'             )
        ->element_exists('form input[name="gathering_name"]'          )
        ->element_exists('form input[name="gathering_date"]'          )
        ->element_exists('form input[name="gathering_time"]'          )
        ->element_exists('form textarea[name="gathering_description"]')
        ->element_exists('form input[type="submit"]'                  )
        ->text_like(h2 => qr/Book New Event/)
};

done_testing();
