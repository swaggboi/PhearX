package PhearX::Model::Gathering;

use Mojo::Base -base, -signatures;

has 'pg';

sub pg_version($self) {
    $self->pg->db->query(<<~'END_SQL')->hash->{'pg_version'}
        SELECT version() AS pg_version;
       END_SQL
}

1;
