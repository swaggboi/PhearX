package PhearX::Controller::Gathering;

use Mojo::Base 'Mojolicious::Controller', -signatures;

sub version($self) {
    my $pg_version = $self->gathering->pg_version;

    $self->stash(pg_version => $pg_version);

    $self->render;
}

sub book($self) {
    #my $author_name           = $self->param('author_name');
    #my $gathering_name        = $self->param('gathering_name');
    #my $gathering_description = $self->param('gathering_description');
    #my $gathering_date        = $self->param('gathering_date');
    #my $gathering_time        = $self->param('gathering_time');
    #my $gathering_datetime    = "$gathering_date $gathering_time";

    $self->render;
}

1;
