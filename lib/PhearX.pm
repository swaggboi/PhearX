package PhearX;

use Mojo::Base 'Mojolicious', -signatures;
use Mojo::Pg;

# Local libs
use PhearX::Model::Gathering;

sub startup ($self) {
    # Plugins
    my $config = $self->plugin('NotYAMLConfig');

    # Helpers
    $self->helper(pg => sub ($c) {
        state $pg = Mojo::Pg->new($c->config->{$self->mode}{'pg_string'})
    });

    $self->helper(gathering => sub ($c) {
        state $gathering = PhearX::Model::Gathering->new(pg => $c->pg)
    });

    # Config stuff
    $self->pg->migrations->from_dir('migrations')->migrate(1);

    $self->secrets($config->{secrets});

    # Begin routing
    my $r = $self->routes;

    $r->get('/', sub ($c) { $c->redirect_to('pg_version') });

    $r->get('/version')->to('Gathering#version')->name('pg_version');

    $r->any('/book')->to('Gathering#book')->name('book_gathering');
}

1;
