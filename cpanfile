requires 'Mojolicious';
requires 'Mojo::Pg';
#requires 'Mojolicious::Plugin::TagHelpers::Pagination';
#requires 'Mojolicious::Plugin::AssetPack';
#requires 'Crypt::Passphrase::Argon2';
requires 'Perl::Critic::Community';
