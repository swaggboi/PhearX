CREATE TABLE IF NOT EXISTS gatherings (
                gathering_id SERIAL PRIMARY KEY,
                booking_date TIMESTAMPTZ DEFAULT NOW(),
              gathering_date TIMESTAMPTZ NOT NULL,
            gathering_author VARCHAR(64),
             gathering_title VARCHAR(256),
       gathering_description VARCHAR(2048),
               hidden_status BOOLEAN NOT NULL,
              flagged_status BOOLEAN NOT NULL
);
