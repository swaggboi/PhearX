# PhearX

A simple event coordination web app.

## Run it

    morbo -w lib/ -w public/ -w script/ -w templates/ script/phear_x

## TODOs

1. Method for ingesting the form data
1. Validation for form input
1. View that renders booked gatherings
1. Action for user to flag gathering
